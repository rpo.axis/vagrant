#!/bin/bash

# update centOS with any patches
yum update -y --exclude=kernel

# tools
yum install -y nano vim git unzip tmux wget

# apache web server
yum install -y httpd httpd-devel httpd-tools
systemctl enable httpd.service
systemctl start httpd.service

# PHP
yum install -y php php-cli php-common php-devel php-mysql

# MariaDB
yum install -y mariadb mariadb-server mariadb-devel mariadb-cli
systemctl enable mariadb.service
systemctl start mariadb.service
mysql -u root -e "SHOW DATABASES"

# Download starter content

cd /var/www/html
wget -q https://gitlab.com/rpo.axis/vagrant/raw/master/file/index.html
wget -q https://gitlab.com/rpo.axis/vagrant/raw/master/file/info.php


systemctl restart httpd.service


