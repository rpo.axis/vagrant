#!/bin/bash

# ubuntu docker instalation

# First, update your existing list of packages:
apt -y update

# Next, install a few prerequisite packages which let apt use packages over HTTPS:
apt -y install apt-transport-https ca-certificates curl software-properties-common wget vim ranger tmux glances

# Then add the GPG key for the official Docker repository to your system:
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

# Add the Docker repository to APT sources:
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"

# Next, update the package database with the Docker packages from the newly added repo:
apt -y update

# Make sure you are about to install from the Docker repo instead of the default Ubuntu repo:
apt-cache policy docker-ce

# Finally, install Docker:
apt -y install docker-ce

# Docker should now be installed, the daemon started, and the process enabled to start on boot. Check that it's running:
systemctl status docker

# We'll check the current release and if necessary, update it in the command below:
# https://github.com/docker/compose/releases
curl -Ls https://github.com/docker/compose/releases/download/1.24.0-rc3/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose

# Next we'll set the permissions:
chmod +x /usr/local/bin/docker-compose

# Then we'll verify that the installation was successful by checking the version:
docker-compose --version

# permition for user vagrant
usermod -aG docker vagrant

# nameservers
echo "nameserver 8.8.8.8" >> /etc/resolv.conf
